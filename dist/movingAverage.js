"use strict";

var _regression = _interopRequireDefault(require("regression"));

var _decimal = require("decimal.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MovingAverage =
/*#__PURE__*/
function () {
  function MovingAverage(dataset, period) {
    var rgSlopeOptions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
      order: 2,
      precision: 10
    };

    _classCallCheck(this, MovingAverage);

    this.dataset = dataset;
    this.period = period;
    this.options = rgSlopeOptions;
  }

  _createClass(MovingAverage, [{
    key: "sumDataPoints",
    value: function sumDataPoints(dataPointsArray) {
      var sum = 0;
      return dataPointsArray.reduce();
    }
  }, {
    key: "getMaDataSet",
    value: function getMaDataSet() {
      if (!this._maDataSet) {
        var dataset = this.dataset;
        var maDataSet = [];
        var length = dataset.length;
        var p = this.period - 1; // zero index

        for (var i = p; i < length; i++) {
          var start = i - p;
          var slice = dataset.slice(start, start + this.period);
          maDataSet.push(slice.reduce(function (val, cur) {
            return val + cur;
          }) / this.period);
        }

        this._maDataSet = maDataSet;
      }

      return this._maDataSet;
    }
  }, {
    key: "getRegressionLine",
    value: function getRegressionLine() {
      if (!this.regLine) {
        this.regLine = _regression["default"].linear(this.getMaDataSet(), this.options);
      }

      return this.regLine;
    }
  }, {
    key: "isBelow",
    value: function isBelow(x, y) {
      var equation = this.getRegressionLine().equation;
      return y <= equation[0] * x + equation[1];
    }
  }, {
    key: "matchPrecision",
    value: function matchPrecision(x) {
      return this.model.flattenXPrecision(x);
    }
  }, {
    key: "isAbove",
    value: function isAbove(x, y) {
      var equation = this.getRegressionLine().equation;
      return y > equation[0] * x + equation[1];
    }
  }, {
    key: "getRegressionSlope",
    value: function getRegressionSlope() {
      return this.getRegressionLine().equation[0];
    }
  }]);

  return MovingAverage;
}();

module.exports = MovingAverage;