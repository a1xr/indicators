"use strict";

var _decimal = require("decimal.js");

/*
    Volatility Adjusted Position Units
*/
module.exports = function (accountValue, atr, dollarsPerPoint) {
  var av = new _decimal.Decimal(av);
  var n = new _decimal.Decimal(atr);
  var dp = new _decimal.Decimal(dollarsPerPoint);
  return av.times(.01).dividedBy(n.times(dp));
};