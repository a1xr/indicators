"use strict";

var _regression = _interopRequireDefault(require("regression"));

var _decimal = require("decimal.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var LinearRegressionChannelIndicator =
/*#__PURE__*/
function () {
  function LinearRegressionChannelIndicator(model) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
      order: 2,
      precision: 10
    };

    _classCallCheck(this, LinearRegressionChannelIndicator);

    this.model = model;
    this.options = options;
  }

  _createClass(LinearRegressionChannelIndicator, [{
    key: "getHighRegressionLine",
    value: function getHighRegressionLine() {
      if (!this.highLine) {
        this.highLine = _regression["default"].linear(this.model.data.high, this.options);
      }

      return this.highLine;
    }
  }, {
    key: "getLowRegressionLine",
    value: function getLowRegressionLine() {
      if (!this.lowLine) {
        this.lowLine = _regression["default"].linear(this.model.data.low, this.options);
      }

      return this.lowLine;
    }
  }, {
    key: "getCloseRegressionLine",
    value: function getCloseRegressionLine() {
      if (!this.closeLine) {
        this.closeLine = _regression["default"].linear(this.model.data.close, this.options);
      }

      return this.closeLine;
    }
  }, {
    key: "getOpenRegressionLine",
    value: function getOpenRegressionLine() {
      if (!this.openLine) {
        this.openLine = _regression["default"].linear(this.model.data.open, this.options);
      }

      return this.openLine;
    }
  }, {
    key: "isBelowChannel",
    value: function isBelowChannel(x, y) {
      var equation = this.getLowRegressionLine().equation;
      return y <= equation[0] * x + equation[1];
    }
  }, {
    key: "matchPrecision",
    value: function matchPrecision(x) {
      return this.model.flattenXPrecision(x);
    }
  }, {
    key: "isAboveChannel",
    value: function isAboveChannel(x, y) {
      var equation = this.getHighRegressionLine().equation;
      return y > equation[0] * x + equation[1];
    }
  }, {
    key: "getChannelSlope",
    value: function getChannelSlope() {
      return this.getCloseRegressionLine().equation[0];
    }
  }, {
    key: "getChannelHeight",
    value: function getChannelHeight() {
      var lowEq = this.getLowRegressionLine().equation;
      var highEq = this.getHighRegressionLine().equation;
      var lastDp = this.model.data.close[this.model.data.close.length - 1];
      var lastX = lastDp[0];
      var lastY = lastDp[1];
      var y1 = new _decimal.Decimal(lowEq[0]).times(lastX).plus(lowEq[1]);
      var y2 = new _decimal.Decimal(highEq[0]).times(lastX).plus(highEq[1]);
      var decimalPlaces = new _decimal.Decimal(lastY).decimalPlaces();
      var lot1, lot2;

      if (y1.lessThan(1) && y2.lessThan(1)) {
        return y2.d[0] - y1.d[0];
      } else {
        return y2.minus(y1).times(new _decimal.Decimal(10).toPower(decimalPlaces));
      }
    }
  }, {
    key: "getNumTouchesBottom",
    value: function getNumTouchesBottom() {
      var dataset = this.model.data.low;
      var touches = 0;

      for (var i = 0; i < dataset.length; i++) {
        var dp = dataset[i];

        if (this.isBelowChannel(dp[0], dp[1])) {
          touches++;
        }
      }

      return touches;
    }
  }]);

  return LinearRegressionChannelIndicator;
}();

module.exports = LinearRegressionChannelIndicator;