"use strict";

var _indicator = _interopRequireDefault(require("./indicator"));

var _binanceModel = _interopRequireDefault(require("../linearRegressionChannel/binanceModel"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = {
  ModifiedLinearRegressionChannelIndicator: _indicator["default"],
  LinearRegressionChannelModel: _binanceModel["default"]
};