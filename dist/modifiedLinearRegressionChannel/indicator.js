"use strict";

var _linearRegressionChannel = _interopRequireDefault(require("../linearRegressionChannel"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ModifiedLinearRegressionChannelIndicator =
/*#__PURE__*/
function (_LinearRegressionChan) {
  _inherits(ModifiedLinearRegressionChannelIndicator, _LinearRegressionChan);

  function ModifiedLinearRegressionChannelIndicator(model) {
    var _this;

    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
      order: 2,
      precision: 5
    };

    _classCallCheck(this, ModifiedLinearRegressionChannelIndicator);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ModifiedLinearRegressionChannelIndicator).call(this, model, options));
    _this.ANALYSIS_TYPES = {
      SPIKE: 'SPIKE',
      NAIL: 'NAIL',
      UP: 'UP',
      DOWN: 'DOWN'
    };
    return _this;
  }

  _createClass(ModifiedLinearRegressionChannelIndicator, [{
    key: "getTailLine",
    value: function getTailLine() {
      if (!this.tailLine) {
        var data = _get(_getPrototypeOf(ModifiedLinearRegressionChannelIndicator.prototype), "model", this).data.close.slice(0, Math.floor(_get(_getPrototypeOf(ModifiedLinearRegressionChannelIndicator.prototype), "model", this).data.close.length / 2));

        this.tailLine = regression.linear(data, this.options);
      }

      return this.tailLine;
    }
  }, {
    key: "getHeadLine",
    value: function getHeadLine() {
      if (!this.headLine) {
        var data = _get(_getPrototypeOf(ModifiedLinearRegressionChannelIndicator.prototype), "model", this).data.close.slice(Math.floor(_get(_getPrototypeOf(ModifiedLinearRegressionChannelIndicator.prototype), "model", this).data.close.length / 2), _get(_getPrototypeOf(ModifiedLinearRegressionChannelIndicator.prototype), "model", this).data.close.length);

        this.headLine = regression.linear(data, this.options);
      }

      return this.headLine;
    }
  }, {
    key: "getTrendAnalysis",
    value: function getTrendAnalysis() {
      var tlSlope = this.getTailLine().equation[0];
      var hlSlope = this.getHeadLine().equation[0];

      if (tlSlope > 0 && hlSlope > 0) {
        return this.ANALYSIS_TYPES.UP;
      } else if (tlSlope < 0 && hlSlope < 0) {
        return this.ANALYSIS_TYPES.DOWN;
      } else if (tlSlope > 0 && hlSlope < 0) {
        return this.ANALYSIS_TYPES.SPIKE;
      } else {
        return this.ANALYSIS_TYPES.NAIL;
      }
    }
  }]);

  return ModifiedLinearRegressionChannelIndicator;
}(_linearRegressionChannel["default"]);

module.exports = ModifiedLinearRegressionChannelIndicator;