"use strict";

var _atr = require("./atr.js");

var _linearRegressionChannel = _interopRequireDefault(require("./linearRegressionChannel"));

var _movingAverage = _interopRequireDefault(require("./movingAverage"));

var _vapUnits = _interopRequireDefault(require("./vapUnits"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = {
  TrueRange: _atr.TrueRange,
  AverageTrueRange: _atr.AverageTrueRange,
  LinearRegressionChannelIndicator: _linearRegressionChannel["default"],
  MovingAverage: _movingAverage["default"],
  VapUnits: _vapUnits["default"]
};