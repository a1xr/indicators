import {TrueRange, AverageTrueRange} from './atr.js';
import LinearRegressionChannelIndicator from './linearRegressionChannel';
import MovingAverage from './movingAverage';
import VapUnits from './vapUnits';


module.exports = {
    TrueRange : TrueRange,
    AverageTrueRange : AverageTrueRange,
    LinearRegressionChannelIndicator : LinearRegressionChannelIndicator,
    MovingAverage : MovingAverage,
    VapUnits : VapUnits
}

