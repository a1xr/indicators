import regression from 'regression';
import {Decimal} from 'decimal.js';

class LinearRegressionChannelIndicator {
    constructor(model, options = {
        order: 2,
        precision: 10,
      }){
        this.model = model;
        this.options = options;
    }

    getHighRegressionLine(){
        if(!this.highLine){
            this.highLine = regression.linear(this.model.data.high, this.options);
        }
        return this.highLine;
    }

    getLowRegressionLine(){
        if(!this.lowLine){
            this.lowLine = regression.linear(this.model.data.low, this.options);
        }
        return this.lowLine;    
    }

    getCloseRegressionLine(){
        if(!this.closeLine){
            this.closeLine = regression.linear(this.model.data.close, this.options);
        }
        return this.closeLine;    
    }

    getOpenRegressionLine(){
        if(!this.openLine){
            this.openLine = regression.linear(this.model.data.open, this.options);
        }
        return this.openLine;
    }

    isBelowChannel(x,y){
        var equation = this.getLowRegressionLine().equation;
        return y <= equation[0] * x + equation[1];
    }

    matchPrecision(x){
        return this.model.flattenXPrecision(x);
    }

    isAboveChannel(x,y){
        var equation = this.getHighRegressionLine().equation;
        return y > equation[0] * x + equation[1];
    }

    getChannelSlope(){
        return this.getCloseRegressionLine().equation[0];
    }

    getChannelHeight(){
        var lowEq = this.getLowRegressionLine().equation;
        var highEq = this.getHighRegressionLine().equation;
        var lastDp = this.model.data.close[this.model.data.close.length - 1];
        var lastX = lastDp[0];
        var lastY = lastDp[1];


        var y1 = new Decimal(lowEq[0]).times(lastX).plus(lowEq[1]);
        var y2 = new Decimal(highEq[0]).times(lastX).plus(highEq[1]);
        var decimalPlaces = new Decimal(lastY).decimalPlaces();

        var lot1, lot2;

        if(y1.lessThan(1) && y2.lessThan(1)){
            return y2.d[0] - y1.d[0];
        }else{
            return y2.minus(y1).times(new Decimal(10).toPower(decimalPlaces));
        }
    }

    getNumTouchesBottom(){
        var dataset = this.model.data.low;
        var touches = 0;
        for(var i = 0; i < dataset.length; i++){
            var dp = dataset[i];
            if(this.isBelowChannel(dp[0], dp[1])){
                touches++;
            }
        }
        return touches;
    }
}

module.exports = LinearRegressionChannelIndicator;
